<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $guarded = [];

    public function brand()
    {
        return $this->hasOne(Brands::class, 'id', 'brand_id');
    }
}
