<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifiedSubscriber extends Model
{
    protected $guarded = [];
}
