<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalVerifiedSubscribers extends Model
{
    protected $guarded = [];
}
