<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Message;
use App\User;
use Twilio\Rest\Client as twilioClient;
use Illuminate\Bus\Batchable;
use App\VerifiedSubscriber;

class SendMessage implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subscriber, $file, $twilio;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($verifiedSubscriber, $fileName)
    {
        $this->subscriber = $verifiedSubscriber;
        $this->file = $fileName;
        $sid = getenv("TWILIO_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $this->twilio = new twilioClient($sid, $token);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        // dd($this->subscriber, $this->file);
        
        $phone_number = $this->twilio->lookups->v1->phoneNumbers($this->subscriber->number)
        ->fetch(["type" => ["carrier"]]);

        if($phone_number->carrier['type'] == "mobile") {
            $verifiedSubscriber = VerifiedSubscriber::where('file_name', $this->file)->where('number', $this->subscriber->number)->first();
            $verifiedSubscriber->valid = 1;
            $verifiedSubscriber->line_type = $phone_number->carrier['type'];
            $verifiedSubscriber->country_code = $phone_number->countryCode;
            $verifiedSubscriber->carrier = $phone_number->carrier['name'];
            $verifiedSubscriber->save();

            // $verified += 1;
        }
        
        return;
    }
}
