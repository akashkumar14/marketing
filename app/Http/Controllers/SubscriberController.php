<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
// use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use DB;

class SubscriberController extends Controller
{
    public function verifyNumber() {
        $numverify_access_key = getenv("numverify_access_key");

        $subscribers = Subscriber::all()->take(100);
        // dd($subscribers);

        $validatedNumbers = [];
        foreach($subscribers as $subscriber) {
            $response = Http::get('https://apilayer.net/api/validate?access_key=' . $numverify_access_key . '&number=1' . $subscriber->number);
            $data= $response->json();
            
            // return $data;
            
            if($data['valid'] == true) {
                $subscriber->valid = 1;
                $subscriber->line_type = $data['line_type'];
                $subscriber->local_format = $data['local_format'];
                $subscriber->international_format = $data['international_format'];
                $subscriber->country_code = $data['country_code'];
                $subscriber->country_name = $data['country_name'];
                $subscriber->location = $data['location'];
                $subscriber->carrier = $data['carrier'];
                $subscriber->save();
            }
            else if($data['valid'] == false) {
                $subscriber->delete();
            }
        }

        dd($subscribers);
    }

    public function show(Subscriber $subscriber)
    {
        // $subscribers = DB::table('subscribers')
        //     ->leftJoin('brands', 'subscribers.brand_id', '=', 'brands.id')
        //     ->select('subscribers.*', 'brands.name as brand_name')
        //     // ->orderby('subscribers.id','desc')
        //     ->paginate(15);

        $subscribers = Subscriber::with('brand')->latest('id')->paginate(15);
        
        $count = DB::table('subscribers')->count();

        return response()->json(['subscribers' => $subscribers, 'count' => $count]);
    }
    
    public function lists() {
        $lists = Subscriber::groupBy('file_name')->select('subscribers.*', DB::raw('COUNT(subscribers.file_name) as file_count'))->latest('id')->get();

        return $lists;
    }

    public function search(Request $request) {
        
        // $results = DB::table('subscribers')
        // ->leftJoin('brands', 'subscribers.brand_id', '=', 'brands.id') 
        // ->where('subscribers.number', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.id', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.email', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.name', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.line_type', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.country', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('subscribers.file_name', 'LIKE', '%'.$request->search.'%')
        // ->orwhere('brands.name', 'LIKE', '%'.$request->search.'%')
        // ->select('subscribers.*', 'brands.name as brand_name')
        // ->get();

        $results=Subscriber::with('brand')
        ->where('number', 'LIKE', '%' . $request->search. '%')
        ->orWhere('id', 'LIKE', '%' . $request->search. '%')
        ->orWhere('email', 'LIKE', '%' . $request->search. '%')
        ->orWhere('name', 'LIKE', '%' . $request->search. '%')
        ->orWhere('file_name', 'LIKE', '%' . $request->search. '%')
        ->orWhereHas('brand', function($q) use($request) {
            $q->where('name', 'LIKE', '%' . $request->search. '%');
        })
        ->get();
        
        return $results;
    }
}
