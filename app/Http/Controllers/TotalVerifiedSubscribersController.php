<?php

namespace App\Http\Controllers;

use App\TotalVerifiedSubscribers;
use Illuminate\Http\Request;

class TotalVerifiedSubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TotalVerifiedSubscribers  $totalVerifiedSubscribers
     * @return \Illuminate\Http\Response
     */
    public function show(TotalVerifiedSubscribers $totalVerifiedSubscribers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TotalVerifiedSubscribers  $totalVerifiedSubscribers
     * @return \Illuminate\Http\Response
     */
    public function edit(TotalVerifiedSubscribers $totalVerifiedSubscribers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TotalVerifiedSubscribers  $totalVerifiedSubscribers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TotalVerifiedSubscribers $totalVerifiedSubscribers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TotalVerifiedSubscribers  $totalVerifiedSubscribers
     * @return \Illuminate\Http\Response
     */
    public function destroy(TotalVerifiedSubscribers $totalVerifiedSubscribers)
    {
        //
    }
}
