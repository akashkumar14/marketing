<?php

namespace App\Http\Controllers;

use App\Brands;
use App\Subscriber;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Brands::all();
    }
    
    public function updateBrand(Request $request) {
        // return $request;
        // if($request->brand == "null" or $request->list == "null") {
        //     abort(403, 'Invalid data');
        // }

        $data = Subscriber::where('file_name', $request->list)->get();
        foreach($data as $item) {
            $item->brand_id = $request->brand;
            $item->save();
        }
        return 1;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Brands;
        $data->name = $request->name;
        $data->save();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function show(Brands $brands)
    {
        $brands = Brands::all();

        return response()->json(['brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $data = Brands::findorfail($id);
        $data->name = $request->name;
        $data->save();

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Brands::findorfail($id)->delete();
        return 1;
    }
}
