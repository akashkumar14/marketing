<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Subscriber;
use App\Brands;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Twilio\TwiML\MessagingResponse;
use GuzzleHttp\Client;
use DB;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $message = new Reply;
        $message->body = strtolower($request->Body);
        $message->number = ltrim($request->From, '+');
        
        $subscriber = Subscriber::whereNotNull('brand_id')->where('number', $message->number)->with('brand')->latest()->first();

        $message->brand_id = $subscriber->brand->id;
        $message->name = $subscriber->name;
        $message->email = $subscriber->email;
        $message->file_name = $subscriber->file_name;
        $message->save();
        
        if($message->body == 'stop') {
            $subscriber->delete();
            $message->delete();
        }
        
        if(Str::contains($message->body, "yes")) {
            
            $body = array(
                'cn' => $subscriber->name,
                'em' => $subscriber->email,
                'pn' => $subscriber->number,
                'brand' => $subscriber->brand->name
            );
            
            $client = new Client(['base_uri' => 'https://logoflixdeals.com/utility/api/'] );
              $response = $client->request('POST', 'save.php' ,
                [      'form_params' =>  $body ] );
                
            // return $response->getBody()->getContents();
        }
        
        // return response('Success', 200);
        
        // $response = new MessagingResponse();
        // $response->message("Thank you for contacting us!");
        // print $response;
        
        // if (Subscriber::where('number', $message->number)->exists()) {
        //   $subscriber = Subscriber::where('number', $message->number)->first();
        //   $subscriber->delete();
        // }
        
        // return 1;
    }

    public function destroy(Request $request) {
        
        $replies = count($request->reply_id);
        for ($i=0; $i<$replies; $i++) { 
            
            $reply = Reply::find($request->reply_id[$i]);
            $subscriber = Subscriber::where('number', $reply->number)->first();
            if($subscriber != null) {
                $subscriber->delete();
            }
            $reply->delete();
        }
        
        // $subscriber = Subscriber::where('number', $request->number)->first();
        // $subscriber->delete();

        // $reply = Reply::where('number', $number)->first();
        // $reply->delete();

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        $replies = Reply::with('brand')->latest('id')->paginate(25);

        // $replies = DB::table('replies')
        //     ->leftJoin('subscribers', 'replies.number', '=', 'subscribers.number')
        //     ->leftJoin('brands', 'subscribers.brand_id', '=', 'brands.id')
        //     ->select('replies.*')
        //     ->orderby('replies.id', 'desc')
        //     ->paginate(25);

        return response()->json(['replies' => $replies]);
    }

    public function search(Request $request) {
        
        // $results = DB::table('replies')
        //     ->join('subscribers', 'replies.number', '=', 'subscribers.number')
        //     ->join('brands', 'subscribers.brand_id', '=', 'brands.id')
        //     ->where('replies.body', 'LIKE', '%'.$request->search.'%')
        //     ->orwhere('replies.id', 'LIKE', '%'.$request->search.'%')
        //     ->orwhere('replies.number', 'LIKE', '%'.$request->search.'%')
        //     ->orwhere('subscribers.email', 'LIKE', '%'.$request->search.'%')
        //     ->orwhere('subscribers.name', 'LIKE', '%'.$request->search.'%')
        //     ->orwhere('brands.name', 'LIKE', '%'.$request->search.'%')
        //     ->select('replies.*', 'subscribers.name', 'brands.name as brand_name')
        //     ->get();

        $results=Reply::with('brand')
        ->where('body', 'LIKE', '%' . $request->search. '%')
        ->orWhere('id', 'LIKE', '%' . $request->search. '%')
        ->orWhere('number', 'LIKE', '%' . $request->search. '%')
        ->orWhere('name', 'LIKE', '%' . $request->search. '%')
        ->orWhere('email', 'LIKE', '%' . $request->search. '%')
        ->orWhere('file_name', 'LIKE', '%' . $request->search. '%')
        // ->orWhereHas('subscriber', function($q) use($request) {
        //     $q->where('name', 'LIKE', '%' . $request->search. '%');
        // })
        // ->orWhereHas('subscriber', function($q) use($request) {
        //     $q->where('email', 'LIKE', '%' . $request->search. '%');
        // })
        ->orWhereHas('brand', function($q) use($request) {
            $q->where('name', 'LIKE', '%' . $request->search. '%');
        })
        // ->orWhereHas('subscriber.brand', function($q) use($request) {
        //     $q->where('name', 'LIKE', '%' . $request->search. '%');
        // })
        ->get();
         
        return $results;
    }
}
