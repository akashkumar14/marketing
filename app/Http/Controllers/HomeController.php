<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Subscriber;
use App\VerifiedSubscriber;
use Auth;
use DB;
use Laravel\Cashier\Cashier;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Twilio\Rest\Client as twilioClient;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Exports\VerifiedDataExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\SendMessage;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function verifyCsv(Request $request) {
        set_time_limit(0);
        
        // try {  
            // $sid = getenv("TWILIO_SID");
            // $token = getenv("TWILIO_AUTH_TOKEN");
            // $twilio = new twilioClient($sid, $token);
            
            $validatedFile = request()->validate(['csv' => 'required|mimes:csv,txt']);
            
            global $uploadedFile;
            if ($request->hasFile('csv')) {
                if ($request->file('csv')->isValid()) {
                    $path = $request->csv->move(public_path('verified files'), $request->file('csv')->getClientOriginalName());
                }
            }

            $file = public_path('verified files/' . $request->file('csv')->getClientOriginalName());
            $file = realpath($file);

            $convertedArrayFile = $this->csvToArray($file);

            for ($i = 0; $i < count($convertedArrayFile); $i ++)
            { 
                $verifiedSubscribers = VerifiedSubscriber::create($convertedArrayFile[$i]);
                $verifiedSubscribers->file_name = $request->file('csv')->getClientOriginalName();
                $verifiedSubscribers->save();
                
                // if($verifiedSubscribers->number == "") {
                //     dd($verifiedSubscribers);
                //     $verifiedSubscribers->delete();
                // }
            }

            // $numverify_access_key = getenv("numverify_access_key");

            $verifiedSubscribers = VerifiedSubscriber::where('file_name', $request->file('csv')->getClientOriginalName())->get();
            // dd($subscribers);

            $validatedNumbers = [];
            $errors = [];
            $verified = 0;
            $total = count($verifiedSubscribers);

            $fileName = $request->file('csv')->getClientOriginalName();
            $jobs = [];
            foreach($verifiedSubscribers as $verifiedSubscriber) {
                $jobs[] = new SendMessage($verifiedSubscriber, $fileName);
            }

            $batch = Bus::batch($jobs)->dispatch();
            
            // foreach($verifiedSubscribers as $verifiedSubscriber) {
            //     try{ 
            //         $phone_number = $twilio->lookups->v1->phoneNumbers($verifiedSubscriber->number)
            //         ->fetch(["type" => ["carrier"]]);

            //         // if(isset($phone_number)) {
            //             if($phone_number->carrier['type'] == "mobile") {
            //                 $verifiedSubscriber->valid = 1;
            //                 $verifiedSubscriber->line_type = $phone_number->carrier['type'];
            //                 $verifiedSubscriber->country_code = $phone_number->countryCode;
            //                 $verifiedSubscriber->carrier = $phone_number->carrier['name'];
            //                 $verifiedSubscriber->save();
    
            //                 $verified += 1;
            //             }
            //         // }
                    
            //     }
            //     catch (\Exception $e) {
            //         array_push($errors, $e->getMessage());
            //         continue;
            //     }
            // }

            // $unverifiedSubscribers = VerifiedSubscriber::where('valid', null)->orWhere('line_type', '!=', 'mobile')->orWhere('number', '=', "")->orWhere('number', '=', null)->get();
            // foreach($unverifiedSubscribers as $unverifiedSubscriber) {
            //     $unverifiedSubscriber->delete();
            // }
            // return $unverifiedSubscribers;

            $data = ['errors' => $errors, 'total_count' => $total, 'verified_count' => $verified, 'batch_id' => $batch->id];

            return response()->json(['status' => true, 'message' => 'File verification successful', 'data' => $data]);
        // } catch (\Exception $e) {

        //     return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        // }
        
        
    }
    
    // public function verifyCsv(Request $request) {
    //     // try {  
    //         $validatedFile = request()->validate(['csv' => 'required|mimes:csv,txt']);
            
    //         global $uploadedFile;
    //         if ($request->hasFile('csv')) {
    //             if ($request->file('csv')->isValid()) {
    //                 $path = $request->csv->move(public_path('verified files'), $request->file('csv')->getClientOriginalName());
    //             }
    //         }

    //         $file = public_path('verified files/' . $request->file('csv')->getClientOriginalName());
    //         $file = realpath($file);

    //         $convertedArrayFile = $this->csvToArray($file);

    //         for ($i = 0; $i < count($convertedArrayFile); $i ++)
    //         { 
    //             $verifiedSubscribers = VerifiedSubscriber::create($convertedArrayFile[$i]);
    //             $verifiedSubscribers->file_name = $request->file('csv')->getClientOriginalName();
    //             $verifiedSubscribers->save();
                
    //             // if($verifiedSubscribers->number == "") {
    //             //     dd($verifiedSubscribers);
    //             //     $verifiedSubscribers->delete();
    //             // }
    //         }

    //         $numverify_access_key = getenv("numverify_access_key");

    //         $verifiedSubscribers = VerifiedSubscriber::where('file_name', $request->file('csv')->getClientOriginalName())->get();
    //         // dd($subscribers);

    //         $validatedNumbers = [];
    //         $errors = [];
    //         $verified = 0;
    //         $total = count($verifiedSubscribers);

    //         foreach($verifiedSubscribers as $verifiedSubscriber) {
    //             // try{ 
    //                 $response = Http::get('https://apilayer.net/api/validate?access_key=' . $numverify_access_key . '&number=' . $verifiedSubscriber->number);
    //                 $data= $response->json();
    //                 // dd($data['valid']);
    //                 if(isset($data['valid'])) {
    //                     if($data['valid'] == true) {
    //                         if($data['line_type'] == 'mobile') {
    //                             $verifiedSubscriber->valid = 1;
    //                             $verifiedSubscriber->line_type = $data['line_type'];
    //                             $verifiedSubscriber->local_format = $data['local_format'];
    //                             $verifiedSubscriber->international_format = $data['international_format'];
    //                             $verifiedSubscriber->country_code = $data['country_code'];
    //                             $verifiedSubscriber->country_name = $data['country_name'];
    //                             $verifiedSubscriber->location = $data['location'];
    //                             $verifiedSubscriber->carrier = $data['carrier'];
    //                             $verifiedSubscriber->save();
        
    //                             $verified += 1;
    //                         }
    //                     }
    //                 }
                    
    //                 // if(isset($data['valid'])) {
    //                 //     if($data['valid'] == false) {
    //                 //         $verifiedSubscriber->delete();
    //                 //         $verified -= 1;
    //                 //     }
    //                 // }

    //                 // if(isset($data['valid'])) {
    //                 //     if($data['line_type'] == 'landline') {
    //                 //         $verifiedSubscriber->delete();
    //                 //         $verified -= 1;
    //                 //     }
    //                 // }

    //                 // if(isset($data['valid'])) {
    //                 //     if($verifiedSubscriber->line_type != 'mobile') {
    //                 //         $verifiedSubscriber->delete();
    //                 //         $verified -= 1;
    //                 //     }
    //                 // }
    //             // }
    //             // catch (Exception $e) {
    //             //     array_push($errors, $e->getMessage());
    //             //     continue;
    //             // }
    //         }

    //         $unverifiedSubscribers = VerifiedSubscriber::where('valid', null)->orWhere('line_type', '!=', 'mobile')->orWhere('number', '=', "")->orWhere('number', '=', null)->get();
    //         foreach($unverifiedSubscribers as $unverifiedSubscriber) {
    //             $unverifiedSubscriber->delete();
    //         }
    //         // return $unverifiedSubscribers;

    //         $data = ['errors' => $errors, 'total_count' => $total, 'verified_count' => $verified];

    //         return response()->json(['status' => true, 'message' => 'File verification successful', 'data' => $data]);
    //     // } catch (\Exception $e) {

    //     //     return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
    //     // }
        
        
    // }

    public function checkBatchProgress($id) {
        $batch = Bus::findBatch($id);

        return $batch;
    }

    public function initializeBatch() {
        $artisanCode = Artisan::call('queue:work --stop-when-empty');
    }
    
    public function downloadCSV() {
        return Excel::download(new VerifiedDataExport, 'verified.csv');
    }

    public function importCsvWithoutVerifying(Request $request) {
        // return $request;
        try {
            
            $validatedFile = request()->validate(['csv' => 'required|mimes:csv,txt']);
            // dd($request->file('csv')->getClientOriginalName());
            if(Subscriber::where('file_name', '=', $request->file('csv')->getClientOriginalName())->exists()) {
                // throw ValidationException::withMessages(['error' => 'File already exists! Try another file']);
                return response()->json(['status' => false, 'message' => 'File already exists! Try another file']);
            }
            
            // $subscribers = Subscriber::all();

            //Transfer data from Subscribers to All Subscribers
            // foreach($subscribers as $subscriber) {
            //     // $totalSubscriber = new TotalSubscriber;
            //     // $totalSubscriber->email = $subscriber->email;
            //     // $totalSubscriber->name = $subscriber->name;
            //     // $totalSubscriber->country = $subscriber->country;
            //     // $totalSubscriber->number = $subscriber->number;
            //     // $totalSubscriber->save();

            //     $subscriber->delete();
            // }

            global $uploadedFile;
            if ($request->hasFile('csv')) {
                if ($request->file('csv')->isValid()) {
                    // $uploadedFile = URL::to('/')."/images/products/".$request->file('image')->getClientOriginalName();
                    $path = $request->csv->move(public_path('files'), $request->file('csv')->getClientOriginalName());
                }
            }
            
            $file = public_path('files/' . $request->file('csv')->getClientOriginalName());
            // $file = public_path('lead logo 2021.csv');
            $file = realpath($file);
            // dd($file);
            $customerArr = $this->csvToArray($file);

            for ($i = 0; $i < count($customerArr); $i ++)
            { 
                // dd($customerArr[$i]['EMAIL']);
                $fileUploading = Subscriber::create($customerArr[$i]);
                $fileUploading->file_name = $request->file('csv')->getClientOriginalName();
                $fileUploading->brand_id = $request->brand;
                $fileUploading->save();
            }

            return 1;

            return response()->json(['status' => true, 'message' => 'Support Information', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        // $sid = getenv("TWILIO_SID");
        // $token = getenv("TWILIO_AUTH_TOKEN");
        // $twilio = new twilioClient($sid, $token);

        // $incomingPhoneNumbers = $twilio->incomingPhoneNumbers
        //                        ->read([], 20);

        // $array = [];
        // foreach($incomingPhoneNumbers as $record) {
        //     array_push($array, $record->phoneNumber);
        // }

        // $product = collect($array);
        // Session::push('twilioNumbers', $product);
        return view('admin');
    }

    public function twilioNumbers() {
        
        // $numbers = Session::get('twilioNumbers');
        // return $numbers;
        
        $sid = getenv("TWILIO_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new twilioClient($sid, $token);

        $incomingPhoneNumbers = $twilio->incomingPhoneNumbers->read([], 50);

        $array = [];
        foreach($incomingPhoneNumbers as $record) {
            array_push($array, $record->phoneNumber);
        }

        $numbers = collect($array);
        return $numbers;
    } 

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
