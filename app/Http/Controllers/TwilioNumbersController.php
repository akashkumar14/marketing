<?php

namespace App\Http\Controllers;

use App\TwilioNumbers;
use Illuminate\Http\Request;

class TwilioNumbersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TwilioNumbers  $twilioNumbers
     * @return \Illuminate\Http\Response
     */
    public function show(TwilioNumbers $twilioNumbers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TwilioNumbers  $twilioNumbers
     * @return \Illuminate\Http\Response
     */
    public function edit(TwilioNumbers $twilioNumbers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TwilioNumbers  $twilioNumbers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TwilioNumbers $twilioNumbers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TwilioNumbers  $twilioNumbers
     * @return \Illuminate\Http\Response
     */
    public function destroy(TwilioNumbers $twilioNumbers)
    {
        //
    }
}
