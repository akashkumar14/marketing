<?php

namespace App\Http\Controllers;

use App\VerifiedSubscriber;
use App\TotalVerifiedSubscribers;
use Illuminate\Http\Request;

class VerifiedSubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VerifiedSubscriber  $verifiedSubscriber
     * @return \Illuminate\Http\Response
     */
    public function show(VerifiedSubscriber $verifiedSubscriber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VerifiedSubscriber  $verifiedSubscriber
     * @return \Illuminate\Http\Response
     */
    public function edit(VerifiedSubscriber $verifiedSubscriber)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VerifiedSubscriber  $verifiedSubscriber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VerifiedSubscriber $verifiedSubscriber)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VerifiedSubscriber  $verifiedSubscriber
     * @return \Illuminate\Http\Response
     */
    public function destroy(VerifiedSubscriber $verifiedSubscriber)
    {
        $subscribers = VerifiedSubscriber::all();

        // Transfer data from Subscribers to All Subscribers
        foreach($subscribers as $subscriber) {
            
            $newData = TotalVerifiedSubscribers::firstOrCreate(
                ['number' => $subscriber->number],
                [
                    'name' => $subscriber->name,
                    'country' => $subscriber->country,
                    'valid' => $subscriber->valid,
                    'email' => $subscriber->email,
                    'line_type' => $subscriber->line_type,
                    'file_name' => $subscriber->file_name,
                    'local_format' => $subscriber->local_format,
                    'international_format' => $subscriber->international_format,
                    'country_code' => $subscriber->country_code,
                    'country_name' => $subscriber->country_name,
                    'location' => $subscriber->location,
                    'carrier' => $subscriber->carrier,
                ]
            );

            $subscriber->delete();
        }
    }
}
