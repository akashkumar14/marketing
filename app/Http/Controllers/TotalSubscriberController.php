<?php

namespace App\Http\Controllers;

use App\TotalSubscriber;
use App\Subscriber;
use Illuminate\Http\Request;

class TotalSubscriberController extends Controller
{
    public function transferData() {
        $subscribers = Subscriber::all();

        foreach($subscribers as $subscriber) {
            $totalSubscriber = new TotalSubscriber;
            $totalSubscriber->email = $subscriber->email;
            $totalSubscriber->name = $subscriber->name;
            $totalSubscriber->country = $subscriber->country;
            $totalSubscriber->number = $subscriber->number;
            $totalSubscriber->save();

            $subscriber->delete();
        }

        return 1;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TotalSubscriber  $totalSubscriber
     * @return \Illuminate\Http\Response
     */
    public function show(TotalSubscriber $totalSubscriber)
    {
        $totalSubscribers = TotalSubscriber::all();

        return response()->json(['total_subscribers' => $totalSubscribers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TotalSubscriber  $totalSubscriber
     * @return \Illuminate\Http\Response
     */
    public function edit(TotalSubscriber $totalSubscriber)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TotalSubscriber  $totalSubscriber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TotalSubscriber $totalSubscriber)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TotalSubscriber  $totalSubscriber
     * @return \Illuminate\Http\Response
     */
    public function destroy(TotalSubscriber $totalSubscriber)
    {
        //
    }
}
