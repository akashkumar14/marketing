<?php

namespace App\Http\Controllers;

use App\Message;
use App\Subscriber;
use App\TotalSubscriber;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Twilio\Rest\Client as twilioClient;
use Illuminate\Support\Facades\Http;

class MessageController extends Controller
{
    public function sendSMS(Request $request) { 
        // return $request->twilioNumber;
        $sid = getenv("TWILIO_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new twilioClient($sid, $token);
        // $number_from = getenv("TWILIO_NUMBER");
        
        //Internal Number
        // $number_to = '+12132055225';

        //US Number
        // $number_to = '+18325266882';

        //Local Number
        // $number_to = '+923363844996';
        
        // $link = 'https://logodesigns.info/';

        $subscribers = Subscriber::where('file_name', $request->list)->get();
        // dd($subscribers);

        $array = [];
        $errors = [];

        if($request->number == "null") {
            foreach($subscribers as $subscriber) {

                //Without 1
                // $number_to = 1 . $subscriber->number;
                
                //With 1
                $number_to = $subscriber->number;
    
                try{
                    
                    $first_name = explode(" ", $subscriber->name);
                    
                    $message = $twilio->messages
                      ->create($number_to, // to
                               [
                                "body" => "Hey, $request->message",
                                "from" => $request->twilioNumber
                               ]
                      );
                }
                catch (\Exception $e) {
                    array_push($errors, $e->getMessage());
                    continue;
                }
            }
        }
        else {
            $messageBody = "Get Exceptional Logo For Your Brand Now, Call Us Back For Premium Logo Service. Avail Now";
            $message = $twilio->messages
                  ->create($request->number, // to
                           [
                            "body" => "Hey, $request->message",
                            "from" => $request->twilioNumber
                           ]
                  );

            print($message->status);
        }
        
        dd($errors);
        return 'Messages Sent!';

        return response()->json($message->status);
        
        print($message->status);
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

}
