<?php

namespace App\Exports;

use App\VerifiedSubscriber;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VerifiedDataExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'ID',
            'Email',
            'Name',
            'Country',
            'Valid',
            'Number',
            'Line_type',
            'File_name',
            'Local_format',
            'International_format',
            'Country_code',
            'Country_name',
            'Location',
            'Carrier',
            'Created_at',
            'Updated_at'
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return VerifiedSubscriber::all();
    }
}
