<?php

namespace App\Exports;

use App\Reply;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Str;

class RepliesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Reply::leftJoin('subscribers', 'replies.number', '=', 'subscribers.number')->where('replies.body', 'LIKE', '%' . 'yes' . '%')->select('replies.*', 'subscribers.name', 'subscribers.email')->get();
    }
}
