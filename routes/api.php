<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Users
Route::get('users', 'HomeController@users');
Route::delete('user/delete/{id}', 'HomeController@deleteUsers');

//Frontend APIs

//Auth
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

// Route::middleware('auth:sanctum')->get('/test', function (Request $request) {
//     return Auth::user();
// });
Route::post('password/email', 'Api\ForgotPasswordController@forgot');

Route::middleware('auth:sanctum')->group(function () {
    // Route::post('password/reset', 'Api\AuthController@resetPassword');

    Route::get('support', 'HomeController@supportInfo');

    //Message APIs
    Route::post('messages', 'MessageController@store');
    Route::get('messages/project', 'MessageController@getAllProjectMessages');

    Route::post('sms/send', 'MessageController@sendSMS');
    Route::post('csv/verify', 'HomeController@verifyCsv');
    Route::post('csv/import/withoutverifying', 'HomeController@importCsvWithoutVerifying');

    Route::get('subscribers/paginated', 'SubscriberController@show');
    Route::get('subscribers/search', 'SubscriberController@search');
    Route::get('subscribers/all', 'TotalSubscriberController@show');

    Route::get('data/transfer', 'TotalSubscriberController@transferData');

    Route::get('number/verify', 'SubscriberController@verifyNumber');

    Route::post('sms/reply', 'ReplyController@index');
    Route::post('reply/delete', 'ReplyController@destroy');

    //Replies
    Route::get('replies', 'ReplyController@show');
    Route::get('replies/search', 'ReplyController@search');

    Route::get('lists', 'SubscriberController@lists');

    Route::get('twilio/numbers', 'HomeController@twilioNumbers');

    Route::get('test', 'HomeController@test');

    Route::get('subscribers/verified/delete/all', 'VerifiedSubscriberController@destroy');

    //Brands
    Route::get('brands', 'BrandsController@index');
    Route::post('brands', 'BrandsController@store');
    Route::post('brands/{id}', 'BrandsController@update');
    Route::delete('brands/{id}', 'BrandsController@destroy');
    Route::post('lists/brand', 'BrandsController@updateBrand');

    Route::get('batch/progress/{id}', 'HomeController@checkBatchProgress');
    Route::get('batch/initialize', 'HomeController@initializeBatch');
});
