<?php

use Illuminate\Support\Facades\Route;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;
use App\Exports\RepliesExport;
use App\Exports\VerifiedDataExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Carbon;
use Twilio\Rest\Client as twilioClient;
use App\Jobs\SendMessage;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\sendSMS;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin');
    // return view('home');
})->middleware('auth');

Route::get('/home', function () {
    return view('admin');
    // return view('home');
})->middleware('auth');



Auth::routes();

Route::get('/home', 'HomeController@admin')->middleware('auth');
Route::get('/', 'HomeController@admin')->middleware('auth');
Route::get('/admin', 'HomeController@admin')->middleware('auth');

Route::get('csv/download', function () {
    return Excel::download(new VerifiedDataExport, Carbon::now('Asia/Karachi') .  '-verified.csv');
});

Route::get('/test', function() {
    $user = User::all();
    $message = "message";
    Notification::send($user, new sendSMS($message));
    return 123;
    $job = SendMessage::dispatch($user, $message);
    return 123;
    return Excel::download(new RepliesExport, 'replies.csv');
    // return (new RepliesExport)->download('replies.csv', \Maatwebsite\Excel\Excel::CSV);
    $filename = 'temp-image.jpg';
    $path = public_path('videos');
    $tempImage = tempnam($path, $filename);
    copy('https://digitalmanager.teamwork.com/?action=viewFile&sd=4f781e-57D65228922CF79F673C3D59F671626CC1CF7140C916BB3DED113903234C00473745700BECEE2AF0FE7AB4FC6149E0C6', $path);

    return response()->download($tempImage, $filename);
    // 
    // $path = 'https://digitalmanager.teamwork.com/?action=viewFile&sd=4f7472-681FA23A07FC5F775DF32501E2BF42DFCEE74982985F484ABB5447FD705689B23745700BECEE2AF0FE7AB4FC6149E0C6';
    // return response()->download($path);
    // return $path;
    // $file_url = $path;
    // header('Content-Type: application/mp4');
    // header("Content-Transfer-Encoding: Binary"); 
    // header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
    // readfile($file_url); 
    
    // $test = App\QuestionAnswers::with('jeopardyCategories')->get();
    $test = App\JeopardyCategory::with('questions')->get();
    return $test;
    
    $user = App\User::find(30);
    Mail::to($user)->send(new TestMail());
    // return $user;
    // $test = '';
    // if($test == null) {
    //     return 1;
    // }
    // elseif($test != null) {
    //     return 0;
    // }
    // return rand(1000, 9999);
    // return $test;
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    return $exitCode;
});
Route::get('getFiles','MessageController@getFiles');
Route::get('project/messages','MessageController@getProjectMessages');
Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
Route::post('password/reset', 'Api\ForgotPasswordController@reset');

Route::get('twilio/numbers', 'HomeController@twilioNumbers');

Route::get('verify', function () {
    $sid = getenv("TWILIO_SID");
    $token = getenv("TWILIO_AUTH_TOKEN");
    $twilio = new twilioClient($sid, $token);

    $phone_number = $twilio->lookups->v1->phoneNumbers("15108675310")
                                    ->fetch(["type" => ["carrier"]]);

    dd($phone_number->carrier['type']);
});