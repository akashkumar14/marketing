require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueContentPlaceholders from 'vue-content-placeholders';
import Swal from 'sweetalert2';

window.Swal = Swal;

Vue.use(VueRouter);
Vue.use(VueContentPlaceholders);
// Vue.use(VueSweetalert2);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const routes = [
    
]

 const router = new VueRouter({
     routes
 })

Vue.prototype.$host_url = "http://thespot.test/api";

const app = new Vue({
    el: '#app',
    router
});