require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VModal from 'vue-js-modal';
import VueFormulate from '@braid/vue-formulate';
import VueContentPlaceholders from 'vue-content-placeholders';
import {
    BootstrapVue,
    BootstrapVueIcons
} from "bootstrap-vue";
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';

import Swal from 'sweetalert2';
import VueCountdownTimer from 'vuejs-countdown-timer';
import VueProgressBar from 'vue-progressbar';

import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
// import BootstrapSelect from 'bootstrap-select';
import Vue from 'vue';

window.Swal = Swal;

// import Vuetify from "../plugins/vuetify"

// import "bootstrap/dist/css/bootstrap.css";
// import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(Loading);
Vue.use(VueRouter);
Vue.use(VModal);
Vue.use(VueFormulate);
Vue.use(VueContentPlaceholders);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(PulseLoader);
Vue.use(VueCountdownTimer);
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    thickness: '5px'
  })
//   Vue.use(BootstrapSelect);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard-component', require('./components/admin/Dashboard.vue').default);

Vue.filter("formattedDateTime", function(dateTime) {
    return moment.utc(dateTime).format('llll')
});

const routes = [{
        path: '/',
        component: require('./components/admin/Dashboard.vue').default,
        props: true
    },
    {
        path: '/example',
        component: require('./components/ExampleComponent.vue').default,
        props: true
    },
    {
        path: '/users',
        component: require('./components/admin/Users.vue').default,
        props: true
    },
    {
        path: '/messages',
        component: require('./components/admin/Messages.vue').default,
        props: true
    },
    {
        path: '/subscribers',
        component: require('./components/admin/Subscribers.vue').default,
        props: true
    },
    {
        path: '/subscribers/all',
        component: require('./components/admin/TotalSubscribers.vue').default,
        props: true
    },
    {
        path: '/numbers',
        component: require('./components/admin/Numbers.vue').default,
        props: true
    },
    {
        path: '/replies',
        component: require('./components/admin/Replies.vue').default,
        props: true
    },
    {
        path: '/brands',
        component: require('./components/admin/Brands.vue').default,
        props: true
    },
    {
        path: '/verify',
        component: require('./components/admin/Verify.vue').default,
        props: true
    },
]

const router = new VueRouter({
    routes
})

Vue.prototype.$host_url = "api";

const app = new Vue({
    el: '#app',
    router,
    // vuetify: Vuetify
});